package preProcess;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Lattice {
	private ArrayList<TracewinRecord> twcArray;

	public static void main(String[] args) throws IOException {
		String dir=	TracewinRecord.getDir();
		Lattice l=new Lattice("Tracewin.dat");

		try {
			l.setSectionNames();
			l.setSubsectionNumbers();
			l.pairSlotEndsWithBegin();		
			l.insertSectionEnds();
			l.insertSubsectionEnds();
			l.addInstruments();
			l.slottifySectionSubsections();
			l.insertTopLevelSlot("srcess"); //Should be "Accelerator"... 
			l.setSlotBeginEndAsMarkers();
			l.makeNamesUnique();
			l.buildHierarchy();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		writeFileLn(dir+"Tracewin2Naming.csv", l.toNameList()); // to be imported into naming database
		writeFileLn(dir+"Tracewin2Lattice.dat",l.toStringArray());	// to be imported into lattice database
	}

	/**
	 * Reads a Tracewin file and generates an array of TracewinRecords.
	 * @param latticeFile 
	 */
	public Lattice(String latticeFile) {
		twcArray =new ArrayList<TracewinRecord>();		
		ArrayList<String> latticeArray= readFileLn(TracewinRecord.getDir()+latticeFile);
		TracewinRecord twc;
		try {
			for (int i = 0; i < latticeArray.size(); i++) {
				twc = new TracewinRecord(latticeArray.get(i), i+1);				
				if(!twc.isComment()) {
					twcArray.add(twc);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Read lines of file and write to a string of array   
	 * @param fileName 
	 * @return 
	 */
	public static ArrayList<String> readFileLn(String fileName){
		String string;
		ArrayList<String> array=new ArrayList<String>();
		BufferedReader latticeInStream = null;
		try {
			latticeInStream = new BufferedReader(new FileReader(fileName));
			while ((string = latticeInStream.readLine()) != null ) {
				array.add(string);
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (latticeInStream != null) {
				try {
					latticeInStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return array;
	}	
	
	/**
	 * Set the section for each tracewinRecord. Section is set by the tag of the beginSection element. 
	 */
	private void setSectionNames() {
		String sectionName = null;
		for (TracewinRecord twc : twcArray) {
			if(twc.isBeginSection()){
				sectionName=twc.getTag();
			}
			twc.setSection(sectionName);
		}
	}

	/**
	 * Set the subsection number for each tracewinRecord. Subsection numbers are reset for a new section and incremented by 1 for each new subsection.  
	 */
	private void setSubsectionNumbers() {
		int subsectionNumber = 0;
		for (TracewinRecord twc : twcArray) {
			if(twc.isBeginSection()){
				subsectionNumber=0;
			} else if(twc.isBeginSubsection() ){			
				subsectionNumber++;
			}
			twc.setSubsection(subsectionNumber);
		}
	}

	/**
	 * Slot end have the same section, subsection, device type, instance number etc. as the beginning of the slot. 
	 * @throws Exception
	 */
	private void pairSlotEndsWithBegin() throws Exception {
		ArrayList<TracewinRecord> beginSlotArray =new ArrayList<TracewinRecord>();
		for (TracewinRecord t : twcArray) {

			if(t.isBeginSlot()){
				beginSlotArray.add(0,t);
			} else if (t.isEndSlot()){
				if(beginSlotArray.isEmpty()) {
					throw new Exception(t +": beginSlot is missing");
				}
				if(t.getSubsectionId()!=beginSlotArray.get(0).getSubsectionId()){					
					t.setSubsection(0);
					beginSlotArray.get(0).setSubsection(0);
				}
				if(t.getSectionId()!= beginSlotArray.get(0).getSectionId()){
					t.setSection(null);
					beginSlotArray.get(0).setSection(null);
				}
				t.setSameNameAs(beginSlotArray.get(0));
				beginSlotArray.remove(0);
			}
		}
		if(!beginSlotArray.isEmpty()) {
			throw new Exception("endSlot is missing " + beginSlotArray.get(0).getTag());
		}
	}

	/**
	 * Insert Section ends into the lattice. Section ends are not indicated in the Tracewin file.  
	 * @throws Exception
	 */
	private void insertSectionEnds() throws Exception {
		int i=0;
		TracewinRecord t;
		TracewinRecord first=null;
		TracewinRecord last=null;
		boolean in=false;
		while(i<twcArray.size()){
			t=twcArray.get(i);
			if (in && t.getSectionId() != first.getSectionId()){
				last= new TracewinRecord();
				last.setEndSection(true);
				last.setSameNameAs(first);
				twcArray.add(i,last);
				in=false;
			}else if(!in && t.isBeginSection()){
				first=t;
				in=true;
			} else if(!in&&last!=null&&t.getSectionId()==first.getSectionId()){
				throw new Exception("Incorrectly nestled section " +t);
			}
			i++;
		}
		if(in){
			last= new TracewinRecord();
			last.setEndSection(true);
			last.setSameNameAs(first);
			twcArray.add(i,last);
			in=false;		
		}
	}

	/**
	 * Insert subsection ends into the lattice. Subsection ends are not indicated in the Tracewin file.  
	 * @throws Exception
	 */
	private void insertSubsectionEnds() throws Exception {
		int i;
		TracewinRecord t;
		TracewinRecord first;
		TracewinRecord last;
		boolean in;
		i=0;
		first=null;
		last=null;
		in=false;
		while(i<twcArray.size()){
			t=twcArray.get(i);
			if (in && t.getSubsectionId()!=first.getSubsectionId()){
				last= new TracewinRecord();
				last.setEndSubsection(true);
				last.setSameNameAs(first);
				twcArray.add(i,last);
				in =false;
			}else if(!in && t.isBeginSubsection()){
				first=t;
				in=true;
			} else if(!in&&last!=null&&t.getSubsectionId()==last.getSubsectionId()){
				throw new Exception("Incorrectly nestled subsection " +t.getSection());
			}
			i++;
		}
	}
	
	/**
	 * Add instruments from separate files. 
	 */
	private void addInstruments()  {
		ArrayList<TracewinRecord> subArray = new ArrayList<TracewinRecord>();
		ArrayList<TracewinRecord> instArray=new ArrayList<TracewinRecord>();

		// make instrument Array
		for (Integer key : TracewinRecord.getSectionRef().keySet()) {
			String section=TracewinRecord.getSectionRef().get(key).getName();
			String dir=TracewinRecord.getDir()+"Instruments/";
			ArrayList<String> instrumentArray = readFileLn(dir+section.toLowerCase()+".dat");
			if(!instrumentArray.isEmpty()){
				// add beginSection
				TracewinRecord first=new TracewinRecord();				
				first.setTag(section);
				first.setSectionId(key);
				first.setBeginSection(true);
				instArray.add(first);
				// add instruments from file
				for (String tag : instrumentArray) {
					TracewinRecord r=new TracewinRecord();				
					// slot beginning and ends are tagged slotname+4begin and slotName + 4end. 
					if(tag.trim().endsWith("4begin")){
						tag=tag.replace("4begin", "");
						r.setBeginSlot(true);
					}else if (tag.trim().endsWith("4end")){
						r.setBeginSlot(true);
						tag=tag.replace("4end", "");
					}
					r.setTag(tag.trim());
					r.setDeviceTypeBasedOnTag(r.getTag());
					if(r.isDevice()){ 
						r.setSectionId(key);
						r.setMarker(!r.isCommand());
						instArray.add(r);
					}
				}
				// add endSection
				TracewinRecord last=new TracewinRecord();				
				last.setSameNameAs(first);
				last.setEndSection(true);
				instArray.add(last);
			}

			// insert the instruments into the lattice
			int index=0;
			for (TracewinRecord r : instArray) {
				boolean found=false;			
				search: for (int i = index; i < twcArray.size(); i++) { // search for the same element in lattice
					TracewinRecord twc=twcArray.get(i);
					if(twc.getSectionId()==r.getSectionId()){
						if(twc.isDevice()&& twc.getDeviceTypeId()==r.getDeviceTypeId() 
								||twc.isBeginSection()&&r.isBeginSection()||twc.isEndSection()&&r.isEndSection()){
							index=i;
							found=true;
							r.setSubsectionId(twc.getSubsectionId());
							break search;
						} else if(twc.isEndSection()){
							break search;
						} 
					}
				} 

				if (found){ // add elements from instument list that have not been found in the lattice before the found element. 
					for (TracewinRecord s : subArray) {
						s.setSubsectionId(r.getSubsectionId());
						s.setSectionId(r.getSectionId());
						twcArray.add(index,s);
						index++;
					}
					subArray=new ArrayList<TracewinRecord>();
					index++;
				} else { // elements that have not been found are saved to be added.  
					subArray.add(r); 
				}
			}
		}
	}

	/**
	 * insert beginning and end of top level slot. 
	 */
	private void insertTopLevelSlot(String slotName) {
		TracewinRecord first = new TracewinRecord();
		first.setBeginSlot(true);
		first.setTag(slotName);
		twcArray.add(0,first);
		TracewinRecord last = new TracewinRecord();
		last.setEndSlot(true);
		last.setSameNameAs(first);
		twcArray.add(last);
	}

	/**
	 * Set section/subsection beginning/end as slot beginning/end.
	 */
	private void slottifySectionSubsections() {

		for (TracewinRecord twr : twcArray) {
			if(twr.isBeginSection()||twr.isBeginSubsection()){
				twr.setBeginSlot(true);
			}else if(twr.isEndSection()||twr.isEndSubsection()){
				twr.setEndSlot(true);
			}
		}		
	}
	/**
	 * Set beginning and end of slots as markers
	 */
	private void setSlotBeginEndAsMarkers() {
		for (TracewinRecord twr : twcArray) {
			if(twr.isBeginSlot()|| twr.isEndSlot()){
				twr.setMarker(true);
			}			
		}		
	}

	/**
	 * Set unique quantifiers a b c ... aa ab ... etc. 
	 */
	private void makeNamesUnique() {
		ArrayList<TracewinRecord> subArray =new ArrayList<TracewinRecord>();
		for (TracewinRecord twc : twcArray) {
			subArray.add(twc);
			twc.setQuantifiers(subArray);
		}
	}
	
	/**
	 * Generates slots (and beamlines...)   
	 * @throws Exception 
	 */
	private void buildHierarchy() throws Exception {
		ArrayList<TracewinRecord> subArray=new ArrayList<TracewinRecord>();
		for (TracewinRecord twr : twcArray) {
			subArray.add(twr);
		}
			twcArray=getTracewinArrayWithBeamlines(subArray);
	}

	/**
	 * Returns a tracewing array with beamlines added.   
	 * @param tArray
	 * @return
	 * @throws Exception
	 */
	private ArrayList<TracewinRecord> getTracewinArrayWithBeamlines(ArrayList<TracewinRecord> tArray) throws Exception {
		ArrayList<TracewinRecord> result = new ArrayList<TracewinRecord>();  
		ArrayList<TracewinRecord> subArray=new ArrayList<TracewinRecord>();

		TracewinRecord firstRecord=tArray.get(0);
		TracewinRecord lastRecord=tArray.get(tArray.size()-1);

		if(!firstRecord.isBeginSlot() ){
			throw new Exception("fistRecord is not BeginSlot");
		}

		if(!lastRecord.isEndSlot()){
			throw new Exception("Last Record is not EndSlot");
		}
		if(!firstRecord.isSameInstance(lastRecord)){
			throw new Exception("first and last records " +firstRecord.getName()+" and "+ lastRecord.getName()+" are not of the same instance");
		}
		
		result.add(firstRecord);
		tArray.remove(0);
		TracewinRecord t;
		boolean between=false;
		while (!tArray.isEmpty()){
			while(!between && !tArray.isEmpty()){
				t =tArray.get(0);
				if (! t.isBeginSlot()){
					result.add(t);
					tArray.remove(0);
				} else {
					firstRecord=t;
					subArray=new ArrayList<TracewinRecord>();
					subArray.add(t);
					tArray.remove(0);
					between=true;
				} 
			}
			while(between && !tArray.isEmpty()){
				t=tArray.get(0);
				subArray.add(t);
				tArray.remove(0);
				if(t.isEndSlot() && t.isSameInstance(firstRecord)){
					result.addAll(getTracewinArrayWithBeamlines(subArray));
					between=false;
				} 
			}
		}
		result.add(0,makeBeamline(result));
		return result;
	}
	
	/**
	 * creates a slot with elements in the subarray.
	 * @param subArray
	 * @return
	 * @throws Exception 
	 */
	private TracewinRecord makeBeamline(ArrayList<TracewinRecord> subArray) throws Exception {
		TracewinRecord firstRecord=subArray.get(0);
		TracewinRecord lastRecord=subArray.get(subArray.size()-1);
		if(!firstRecord.isBeginSlot() ){
			throw new Exception("fistRecord is not BeginSlot");
		}
		if(! lastRecord.isEndSlot()){
			throw new Exception("Last Record is not EndSlot");
		}
		if(! firstRecord.isSameInstance(lastRecord)){
			throw new Exception(firstRecord+ " and " + lastRecord +" are not of the same instance");
		}

		TracewinRecord slot=new TracewinRecord();
		slot.setSameNameAs(firstRecord);
		slot.setSlot(true);
		String s = "";
		for (TracewinRecord twc : subArray) {
			if(twc.getParent()==null){ // if parent is not null then element is already in another slot (or beamline) and shall not be added again. 
				s=s+" "+twc.getName();
				twc.setParent(slot.getName());
			}
		}
		slot.setParameters(s.trim().split(" "));
		return slot;
	}	
	
	
	/**
	 * Writes records of the Array<String> array to a file named fileName   
	 * @param fileName
	 * @param array
	 */
	public static void writeFileLn(String fileName, ArrayList<String> array){
		PrintWriter latticeOutStream=null;
		try {
			latticeOutStream = new PrintWriter(new FileWriter(fileName));
			for (String string : array) {
				latticeOutStream.println(string);
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			if (latticeOutStream != null) {
				latticeOutStream.close();
			}
		}
	}
	
	
	/**
	 * 
	 * @return
	 */
	private ArrayList<String> toNameList() {
		ArrayList<String> nameArray=new ArrayList<String>();
		int i=0;
		String delimiter=", ";
		String header="ID"+delimiter+ "DeviceTypeID"+ delimiter+ "SectionID"+ delimiter+ "SubsectionID"+delimiter+ "Quantifier"+delimiter+ 
				"SortOrder"+delimiter+ "Name";
		nameArray.add(header);
		for (TracewinRecord twc : twcArray) {
			if(twc.isDevice()){
				i++;
				try {
					nameArray.add(twc.toElementString(i));
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}
		return nameArray;
	}
	
	/**
	 * Returns twcArray as a string array 
	 * @return
	 */
	public ArrayList<String> toStringArray() {
		ArrayList<String> nameArray = new ArrayList<String>();
		for (TracewinRecord twc : twcArray) {
			try {
				nameArray.add(twc.toString());
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return nameArray;
	}
	
}
