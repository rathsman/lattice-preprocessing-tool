package preProcess;

public class Type {
	private String name=null;
	private int parentId;
	public Type() {
		super();
		
	}
	@Override
	public String toString(){
		return name+", "+ parentId;
	}
	public String getName() {
		return name;
	}
	public int getParentId() {
		return parentId;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
}
