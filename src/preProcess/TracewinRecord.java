package preProcess;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TracewinRecord {
	private static String[] alpha= {"a","b","c","d","e","f","g","h","j","k"};      //,"k","l","m","o","p","q","r","s","t","u","v","x","y","z"};
	private static String begin="_Begin";
	private static String end="_End";
	private static Map<Integer,Type> deviceTypeRef=getMap(getDir()+"ReferenceTables/DeviceTypes.csv");
	private static Map<Integer,Type> disciplineRef=getMap(getDir()+"ReferenceTables/Disciplines.csv");
	private static Map<Integer,Type> sectionRef=getMap(getDir()+"ReferenceTables/Sections.csv");
	private static Map<Integer,Type> subsectionRef=getMap(getDir()+"ReferenceTables/Subsections.csv");
	private static Map<Integer,Type> commandRef=getMap(getDir()+"ReferenceTables/Commands.csv");
	private static Map<Integer,Type> tagRef=getMap(getDir()+"ReferenceTables/Tags.csv");
	private String tag=null;
	private String[] parameters;
	private String comment=null;
	private String parent;
	private String phrase=null;
	private int sectionId=0;
	private int subsectionId=0;
	private int deviceTypeId=0;
	private String quantifier=null;
	private boolean beginSlot=false;
	private boolean endSlot=false;	
	private boolean slot=false;
	private boolean iscomment=false;
	private boolean marker=false;
	private boolean beginSection=false;
	private boolean endSection=false;
	private boolean beginSubsection=false;
	private boolean endSubsection=false;
	private boolean command=false;

	static String getDir(){
		return "/users/karinrathsman/BLT/";		
	}	
	
	/**
	 * Constructor 
	 * @param row
	 */
	TracewinRecord(){
		super();
	}

	/**
	 *Constructor reads a Tracewin command sorts out commands, comments, marker, beamlines, slots, beginning of sections, subsections and slots as well as end of slots. Names, parameters and boolean flags are set.
	 * @param phrase 
	 * @throws Exception 
	 */
	TracewinRecord(String phrase,int row) throws Exception {
		setPhrase(phrase.trim());		
		if(!getPhrase().startsWith(";") && !getPhrase().isEmpty()){
			setCommand(true);
			separateComment();
			separateTag();
			String command=separateCommand();
			separateParameters();
			setDeviceTypeBasedOnCommand(command);
		}else if(beginWith(";beginSection")){
			separateComment();
			if(separateTag()){	
				setBeginSection(true);
			} else {
				setBeginSubsection(true);
			}
		} else if(beginWith(";beginSlot")){
			separateComment();
			separateTag();			
			setBeginSlot(true);
			setDeviceTypeBasedOnTag(getTag());				
		} else if(beginWith(";endSlot")){
			separateComment();			
			setEndSlot(true);
		}else{
			setIsComment(true);
			separateComment();
		}
		setPhrase(phrase);
	}

	/**
	 * The device type is defined by the Tracewin command in combination with parameters values.
	 * @param command
	 */
	private void setDeviceTypeBasedOnCommand(String command) {
		try {
			if(isCommand() ){
				if(command.equals("BEND") && getParameters()!=null){
					if(Double.parseDouble(getParameters()[4])==1 ){
						setDeviceTypeBasedOnTag("dv");
					} else {
						setDeviceTypeBasedOnTag("dh");;
					}
				} else 	if(command.equals("MULTIPOLE")&& getParameters()!=null){
					if(Double.parseDouble(getParameters()[3])<0){
						setDeviceTypeBasedOnTag("octv");;
					} else {
						setDeviceTypeBasedOnTag("octh");;
					}
				} else 	if(command.equals("QUAD")&& getParameters()!=null){
					if( Double.parseDouble(getParameters()[1])<0){ 
						setDeviceTypeBasedOnTag("qv");
					} else {
						setDeviceTypeBasedOnTag("qh");
					}
				} else  {
					int deviceTypeId=getId(command,commandRef);
					setDeviceTypeId(deviceTypeId);
				}
			}
		} catch (Exception e) {
			System.out.println("setDeviceTypeBasedOnCommand "+ e);
		}
	}

	/**
	 * The device type is defined by a tag. True if the tag exists in tag reference table. 
	 * @param tag
	 * @return
	 */
	boolean setDeviceTypeBasedOnTag(String tag) {
		try {
			int deviceTypeId=getId(tag,tagRef);
			setDeviceTypeId(deviceTypeId);
			return true;
		} catch (Exception e) {
			System.out.println("setDeviceTypeBasedOnTag "+ e);
			return false;
		}

	}

	/**
	 * Separates parameters from phrase into an array and set parameters
	 * @return
	 */
	private boolean separateParameters() {
		boolean result;
		if (getPhrase()!=null){
			setParameters(getPhrase().split(" "));
			setPhrase(null);
			result=true;
		} else {
			result=false;
		}
		return result;
	}

	/**
	 * Instructions like ;marker, ;beamline, ;slot specified in the beginning of a command line is trimmed of phrase.
	 * @param begin
	 * @return
	 */
	private boolean beginWith(String begin){
		boolean result;
		if(getPhrase().toLowerCase().startsWith(begin.trim().toLowerCase())){
			result=true;
			setPhrase(getPhrase().substring(begin.length()).trim());
		} else {
			result=false;
		}
		return result;
	}

	/** 
	 * Separates the command from phrase 'command parameters'.
	 * @return
	 */
	private String separateCommand() {
		String divider=" ";
		String command=null;
		if(isCommand() && !getPhrase().isEmpty() ){
			String[] parsed = getPhrase().split(divider,2);
			if(!parsed[0].trim().isEmpty()){
				command=parsed[0].trim().toUpperCase();
				if(parsed.length>1&& !parsed[1].trim().isEmpty())	{
					setPhrase(parsed[1].trim());
				} else {
					setPhrase(null);
				}
			}
		}
		return command;
	}

	/** 
	 * Separates the name or tag from phrase 'name : command parameters' or 'name parameters' and sets the name.
	 * @return
	 */
	private boolean separateTag() {
		boolean result=false;
		String divider;
		if(isCommand()){
			divider=":";
		} else{
			divider=" ";
		}
		if(getPhrase()!=null){
			String[] parsed = getPhrase().split(divider,2);
			if(!parsed[0].trim().isEmpty()){				
				if(parsed.length > 1 && !parsed[1].trim().isEmpty()){
					setTag(parsed[0].trim().toLowerCase());
					result=true;
					setPhrase(parsed[1].trim());				
				} else if(!isCommand()){
					setTag(parsed[0].trim().toLowerCase());
					result=true;
					setPhrase(null);
				}			
			}
		}
		return result;
	}

	/**
	 * separates the comment from phrase  of the form 'string ; comment' and sets the comment.
	 * @return
	 */
	private boolean separateComment() {
		boolean result=false;
		String divider=";";
		if(getPhrase().contains(divider)){			
			String[] parsed = getPhrase().split(divider,2);
			if(!parsed[1].trim().isEmpty())	{
				setComment(parsed[1].trim());
				result=true;
			} else {
				result=false;
			}			
			setPhrase(parsed[0].trim());
		} 
		return result;
	}

	String getComment() {
		return comment;
	}

	void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Returns the command name, according to command reference table.  
	 * @return
	 */
	String getCommand() {
		String command=null;
		if(isCommand() && isDevice()){
			command=commandRef.get(getDeviceTypeId()).getName();
		}
		return command;
	}

	/**
	 * Returns the id of a string from a map
	 * @param name
	 * @param ref
	 * @return
	 * @throws Exception
	 */
	private static int getId(String name, Map<Integer, Type> ref) throws Exception {
		int result=0;
		boolean found=false;
		search:	for (Integer id : ref.keySet()){
			String refName=ref.get(id).getName();
			if(name!=null&&name.equalsIgnoreCase(refName)){
				result=id;
				found=true;
				break search;
			}
		}
		if (found){
			return result;
		} else {
			throw new Exception(name +" was not found");
		}
	}

	boolean isCommand() {
		return command;
	}

	void setCommand(boolean command){
		this.command=command;
	}

	boolean isComment() {
		return iscomment;
	}

	void setIsComment(boolean iscomment) {
		this.iscomment = iscomment;
	}

	private boolean isMarker() {
		return marker;
	}

	void setMarker(boolean marker) {
		this.marker = marker;
	}

	String getTag() {
		return tag;
	}

	void setTag(String name) {
		this.tag = name;
	}

	String[] getParameters() {
		return parameters;
	}

	void setParameters(String[] parameters) {
		this.parameters = parameters;
	}

	String getPhrase() {
		return phrase;
	}

	void setPhrase(String phrase) {
		this.phrase=phrase;
	}

	boolean isSlot() {
		return slot;
	}

	void setSlot(boolean slot) {
		this.slot = slot;
	}

	void setBeginSection(boolean beginSection) {
		this.beginSection=beginSection;
	}

	boolean isBeginSubsection() {
		return beginSubsection;
	}

	void setBeginSubsection(boolean beginSubsection) {
		this.beginSubsection = beginSubsection;
	}

	boolean isBeginSection() {
		return beginSection;
	}

	/**
	 * Returns the section name, based on the sectionId and section reference table
	 * @return
	 */
	String getSection() {
		try{
			return sectionRef.get(getSectionId()).getName();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Sets the sectionId based on a section name. 
	 * @param section
	 * @return
	 */
	boolean setSection(String section) {
		try {
			setSectionId(getId(section,sectionRef));
			return true;
		} catch (Exception e) {
			System.out.println("setSection "+ e);
			return false;
		}
	}

	/**
	 * Retruns the subsection name based on subsectionId and subsection reference table
	 * @return
	 */
	String getSubsection() {
		return subsectionRef.get(getSubsectionId()).getName();
	}

	/**
	 * Set the subsectionId based on the subsection name
	 * @param subsection
	 * @return
	 */
	boolean setSubsection(String subsection) {
		try {
			setSubsectionId(getId(subsection,subsectionRef,getSectionId()));
			return true;
		} catch (Exception e) {
			System.out.println("setSubsection "+ e);
			return false;
		}
	}

	/**
	 * retruns the Id (key) from a reference maps
	 * @param name
	 * @param referenceMap
	 * @param parentID
	 * @return
	 * @throws Exception
	 */
	private static int getId(String name, Map<Integer, Type> referenceMap, int parentID) throws Exception {
		int result=0;
		boolean found=false;
		search:	for (Integer id : referenceMap.keySet()){
			String refName=referenceMap.get(id).getName();
			if(name!=null&&name.equalsIgnoreCase(refName) && parentID==referenceMap.get(id).getParentId()){
				result=id;
				found=true;
				break search;
			}
		}
		if (found){
			return result;
		} else {
			throw new Exception(name +" was not found");
		}

	}

	/**
	 * Returns the discipline based on the disciplineID and discipline reference map.
	 * @return
	 */
	String getDiscipline() {
		return disciplineRef.get(getDisciplineId()).getName();
	}

	String getParent() {
		return parent;
	}

	void setParent(String parent) {
		this.parent = parent;
	}

	@Override
	public String toString(){
		String result;
		if(isCommand()){
			result=getName()+": "+getCommand()+" "+getParameterString();
			if(getComment()!=null) result=result+" ; "+getComment();
		} else if(isSlot()) {
			if(isDevice()){
				result= ";slot "+getName()+" "+getParameterString();
			} else {
				result= ";beamline "+getName()+" "+getParameterString();
			}
		} else if(isMarker()){			
			result=";Marker "+getName();
		} else if(isComment()&& getComment()!=null){
			result=";"+getComment();
		} else {
			result="";
		}
		return result;
	}

	/**
	 * generates a record for the naming database
	 * @param id
	 * @return
	 * @throws Exception
	 */
	String toElementString(int id) throws Exception{
		String result;
		String delimiter=",";
		String quantifier=getQuantifier();
		if(quantifier==null) quantifier="";
		boolean isElement = isDevice() && !isEndSlot()&&!isBeginSlot();

		if(isElement){
			result=id+delimiter+ getDeviceTypeId()+ delimiter+ getSectionId()+ delimiter+ getSubsectionId()+delimiter+quantifier+ delimiter+ 10*id+delimiter+ getName();
		}else{
			throw new Exception("toElementString: "+ getName() + " is not an element");
		}
		return result;
	}

	/**
	 * returns the parameter of type String[] as a single String separated with space (" ") as delimiter.  
	 * @return
	 */
	private String getParameterString() {
		String result=" ";
		if(getParameters()!=null){
			for (int i = 0; i < getParameters().length; i++) {
				result=result + getParameters()[i] +" ";
			}
		}
		return result.trim();
	}

	/**
	 * sets the subsection based on the subsection number
	 * @param subsectionNumber
	 */
	void setSubsection(int subsectionNumber) {
		if (subsectionNumber==0){
			setSubsection(null);
		} else if(subsectionNumber<=9){
			setSubsection("0"+ String.valueOf(subsectionNumber));
		}else{
			setSubsection(String.valueOf(subsectionNumber));
		}		
	}

	/**
	 * return number of leading zeros...
	 * @param x
	 * @param alpha
	 * @return
	 */
	private static int getQuantum(int x,String[] alpha){
		int r;
		int base=alpha.length;
		int n=0;
		while (x>0) {
			n++;
			r= (int) (x/base);
			x=r;
		}
		return n;
	}

	/**
	 * To set quantifiers... 
	 * @param x
	 * @param alpha
	 * @param n
	 * @return
	 */
	private static String getQuantumString(int x,String[] alpha, int n){
		String result = null;
		int base=alpha.length;
		int r;
		int k;
		for (int i = 0; i < n; i++) {
			r= (int) (x/base);
			k= x-r*base;
			x=r;
			if(result==null){
				result = alpha[k];				
			} else {
				result = alpha[k]+result;
			}
		}
		return result;
	}
	void setQuantifier(String quantifier){
		this.quantifier= quantifier;
	}

	/**
	 * sets quantifier of twc to distinguish instances
	 * @param array
	 */
	void setQuantifiers(ArrayList<TracewinRecord> array) {
		ArrayList<TracewinRecord> twcEqual=new ArrayList<TracewinRecord>();
		String instance=getName(null);
		for (TracewinRecord t : array) {			
			if(instance!=null && instance.equals(t.getName(null))){
				twcEqual.add(t); // list of devices with the same name apart from qualifier 
			}
		} 				
		int k=twcEqual.size();
		int n=getQuantum(k-1,alpha);
		for (int j = 0; j < k; j++) {
			String s=getQuantumString(j,alpha,n);
			twcEqual.get(j).setQuantifier(s); // previous are updated so all quantifier has the same length
		}
	}

	/**
	 * Returns the devicetype name based on deviceTypeId and deviceType reference map
	 * @return
	 */
	String getDeviceType() {
		return deviceTypeRef.get(getDeviceTypeId()).getName();
	}

	String getQuantifier() {
		return quantifier;
	}

	/**
	 * Returns a name with the quantifier specified by argument
	 * @param quantifier
	 * @return
	 */
	String getName(String quantifier) {

		// sectionPart
		String sectionPart=null;
		if(getSectionId()!=0){
			sectionPart=getSection();
		}
		// devicePart
		String devicePart=null;
		if(getDeviceTypeId()!=0){
			if(getDisciplineId()!=0) {
				devicePart=getDiscipline()+"_"+getDeviceType();
			}else {
				devicePart=getDeviceType();
			}
		}
		// quantifierPart;
		String quantifierPart=null;
		if (getSubsectionId()!=0 && quantifier!=null) {
			quantifierPart=getSubsection()+quantifier;
		} else if (getSubsectionId()!=0 && quantifier==null) {
			quantifierPart=getSubsection();
		} else if (getSubsectionId() == 0 && quantifier!=null){
			quantifierPart=quantifier;				
		}

		// endpart
		String endPart=null;
		if (isBeginSlot()){
			endPart = begin;
		}else if (isEndSlot()){
			endPart = end;
		}

		// instance
		String instance=null;

		if(sectionPart!=null && devicePart!=null) {
			instance = sectionPart +"-"+ devicePart;
		} else if(sectionPart !=null && devicePart==null) {
			instance = sectionPart;
		} else if(sectionPart ==null && devicePart!=null){
			instance = devicePart;
		} else if(sectionPart==null && devicePart==null && getTag()!=null){
			instance=getTag();
		}

		if(instance!=null && quantifierPart!=null){
			instance = instance+ "-"+quantifierPart;
		}

		if(instance!=null && endPart!=null){
			instance = instance + endPart;
		}

		return instance;
	}
	
	
	/**
	 * Return the deviceName with quantifier.
	 * @return
	 */
	String getName(){
		return getName(getQuantifier());
	}

	boolean isBeginSlot() {
		return beginSlot;
	}

	void setBeginSlot(boolean beginSlot) {
		this.beginSlot = beginSlot;
	}
	boolean isEndSlot() {
		return endSlot;
	}

	void setEndSlot(boolean endSlot) {
		this.endSlot = endSlot;
	}

	/**
	 * Return the subsetion number based on subsection name
	 * @return
	 */
	int getSubsectionNumber() {
		int result=0;
		if(getSubsection()!=null){
			result= Integer.parseInt(getSubsection());
		}
		return result;
	}

	/**
	 * Set same name, except for begin or end of slot. 
	 * @param r
	 */
	void setSameNameAs(TracewinRecord r) {
		setTag(r.getTag());
		setSectionId(r.getSectionId());
		setSubsectionId(r.getSubsectionId());
		setDeviceTypeId(r.getDeviceTypeId());	
		setQuantifier(r.getQuantifier());
	}

	boolean isEndSubsection() {
		return endSubsection;
	}

	void setEndSubsection(boolean endSubsection) {
		this.endSubsection = endSubsection;

	}
	boolean isEndSection() {
		return endSection;
	}

	void setEndSection(boolean endSection) {
		this.endSection = endSection;
	}

	/**
	 * Returns true if two records are identical except for begin or end of slot. 
	 * @param r
	 * @return
	 */
	boolean isSameInstance(TracewinRecord r) {
		boolean result=getTag()==null&&r.getTag()==null || getTag()!=null &&getTag().equals(r.getTag());
		result= result&& (getQuantifier()==null&&r.getQuantifier()==null || getQuantifier()!=null &&getQuantifier().equals(r.getQuantifier()));
		result=result && getSectionId()==r.getSectionId();
		result=result && getSubsectionId() == r.getSubsectionId();
		result=result && getDeviceTypeId() == r.getDeviceTypeId();
		return result;
	}

	int getDeviceTypeId() {
		return deviceTypeId;
	}
	boolean isDevice() {
		return deviceTypeRef.containsKey(getDeviceTypeId());
	}

	private void setDeviceTypeId(int deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
		setCommand(commandRef.containsKey(deviceTypeId));
	}
	int getDisciplineId() {
		return deviceTypeRef.get(getDeviceTypeId()).getParentId();
	}
	int getSectionId() {
		return sectionId;
	}
	int getSubsectionId() {
		return subsectionId;
	}
	void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}
	void setSubsectionId(int subsectionId) {
		this.subsectionId = subsectionId;
	}

	/**
	 * returns a reference map
	 * @param fileName
	 * @return
	 */
	private static Map<Integer, Type> getMap(String fileName) {
		ArrayList<String> deviceTypes=Lattice.readFileLn(fileName);
		Map<Integer,Type> m=new HashMap<Integer,Type>();
		for (String string : deviceTypes) {
			String[] parsed=string.trim().split(",");

			int id=Integer.parseInt(parsed[0]);
			String name=parsed[1].trim();
			String parent=null;
			if(parsed.length>2){
				parent =parsed[2].trim();
			}

			Type d= new Type();		
			d.setName(name);
			if(parent!=null){
				int parentId=Integer.parseInt(parent);
				d.setParentId(parentId);
			}
			m.put(id, d);
		}
		return m;
	}
	static Map<Integer, Type> getSectionRef() {
		return sectionRef;
	}

}
